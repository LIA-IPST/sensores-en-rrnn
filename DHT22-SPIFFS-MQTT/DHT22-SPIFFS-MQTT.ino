// librerías mínimas requeridas
#include <Arduino.h>
#include <Wire.h>
#include <ESP8266WiFi.h>
#include <MQTTClient.h>
#include <FS.h>
#include <ArduinoJson.h>
#include <Hash.h>

// ID de este nodo
String CLIENTID = "Node_" + String( ESP.getChipId() );

// objeto para la WiFi
WiFiClient wiFiClient;

// objeto para MQTT. 256 bytes buffer de I/O
MQTTClient mqtt( 256 );

// datos requerido para la conexion
#define CONFIG_FILE "config.txt"// archivo de configuracion
char WIFI_SSID[65];             // SSID de la WiFi
char WIFI_PASS[65];             // clave de la WiFi
char MQTT_SERVER[128];          // servidor MQTT
char MQTT_PORT[6];              // puerta MQTT
char MQTT_TOPIC[128];           // topico al que publica
char TIME_DELAY[10];            // milisegundos entre cada lectura

// control del código
#define TESTING         0       // 1: modo testing, 0: modo normal
#define DEEP_SLEEP      1       // 0: no usar DeepSleep, 1: usar DeepSleep
unsigned long DELAY;            // tiempo entre cada lectura en ms

// ------------------------------------------
// colocar aquí definiciones para su programa
// ------------------------------------------
#include "DHT.h"
DHT dht( D5, DHT22 );

// ------------------------------------------

void setup()
{
  // inicia monitor serial
  Serial.begin( 9600 );
  Serial.println();
  Serial.println();

  #if TESTING==0
  // iniciamos el sistema de archivos
  SPIFFS.begin();

  // esperamos algunos segundos por posible cambio en la configuración
  Serial.println();
  Serial.println();
  Serial.print( CLIENTID );
  Serial.print( " - Presione la tecla 's' y luego <enter> para reconfigurar:" );
  char line[8];

  if( readLine( line, 7, 2000 ) == 1 && line[0] == 's' && line[1] == 0 )
  {
    Serial.println();
    Serial.print( "Wifi SSID: " );
    if( readLine( WIFI_SSID, sizeof( WIFI_SSID ) - 1 ) )
    {
      Serial.print( "WiFi Clave: " );
      if( readLine( WIFI_PASS, sizeof( WIFI_PASS ) - 1 ) )
      {
        Serial.print( "MQTT Servidor: " );
        if( readLine( MQTT_SERVER, sizeof( MQTT_SERVER ) - 1 ) )
        {
          Serial.print( "MQTT Puerta: " );
          if( readLine( MQTT_PORT, sizeof( MQTT_PORT ) - 1 ) )
          {
            Serial.print( "MQTT Topico:" );
            if( readLine( MQTT_TOPIC, sizeof( MQTT_TOPIC ) - 1 ) )
            {
              Serial.print( "Tiempo entre Lecturas (ms):" );
              if( readLine( TIME_DELAY, sizeof( TIME_DELAY ) - 1 ) )
              {
                File f = SPIFFS.open( CONFIG_FILE, "w" );
                f.println( WIFI_SSID );
                f.println( WIFI_PASS );
                f.println( MQTT_SERVER );
                f.println( MQTT_PORT );
                f.println( MQTT_TOPIC );
                f.println( TIME_DELAY );
                f.close();

                // inicia conexión a la WiFi
                WiFi.setAutoConnect( true );
                WiFi.mode( WIFI_STA );
                WiFi.begin( WIFI_SSID, WIFI_PASS );
              }
            }
          }
        }
      }
    }
  }

  // lee desde el archivo la configuracion requerida
  File f = SPIFFS.open( CONFIG_FILE, "r" );
  if( !f )
  {
    Serial.println();
    Serial.println( "No existe configuracion almacenada, el nodo se reiniciara en 3 segundos ..." );
    delay( 3000 );
    ESP.restart();
  }
  else
  {
    int n;
    if( n = f.readBytesUntil( '\n', WIFI_SSID, sizeof( WIFI_SSID ) ) ) WIFI_SSID[n-1]=0;
    if( n = f.readBytesUntil( '\n', WIFI_PASS, sizeof( WIFI_PASS ) ) ) WIFI_PASS[n-1]=0;
    if( n = f.readBytesUntil( '\n', MQTT_SERVER, sizeof( MQTT_SERVER ) ) ) MQTT_SERVER[n-1]=0;
    if( n = f.readBytesUntil( '\n', MQTT_PORT, sizeof( MQTT_PORT ) ) ) MQTT_PORT[n-1]=0;
    if( n = f.readBytesUntil( '\n', MQTT_TOPIC, sizeof( MQTT_TOPIC ) )  )MQTT_TOPIC[n-1]=0;
    if( n = f.readBytesUntil( '\n', TIME_DELAY, sizeof( TIME_DELAY ) ) ) TIME_DELAY[n-1]=0;
    f.close();

    DELAY = atol( TIME_DELAY );
  }
  strcat( MQTT_TOPIC, "/" );
  strcat( MQTT_TOPIC, CLIENTID.c_str() );

  // asocia el servidor MQTT
  mqtt.begin( MQTT_SERVER, atoi( MQTT_PORT ), wiFiClient );
  #endif

  // ---------------------------------------
  // colocar aquí inicialización de sensores
  // ---------------------------------------
  dht.begin();
  
  // ---------------------------------------

  // requerido para dar tiempo a los sensores
  Serial.println();
  Serial.println( "Iniciando lectura de sensores ..." );
  delay( 500 );
}

void loop()
{
  // transmitiremos en formato JSON
  StaticJsonBuffer<300> jsonBuffer;
  JsonObject& json = jsonBuffer.createObject();
  json["Id"] = CLIENTID;

  // ---------------------------------------
  // colocar aquí la lectura de los sensores
  // ---------------------------------------

  // Sensor DHT22
  float dhtTemperatura;
  float dhtHumedad;
  for( int i=0; i<3; i++ ){
    dhtTemperatura = dht.readTemperature();
    dhtHumedad = dht.readHumidity();
    yield();
    if( !isnan( dhtTemperatura ) && !isnan( dhtHumedad ) )
      break;
    Serial.println( "DHT22 reporta NAN" );
    yield();
    delay( 500 );
  }
  json["Temperatura"] = dhtTemperatura;
  json["Humedad"] = dhtHumedad;
  yield();
  
  // Voltaje de la bateria
  // Agregamos una resistencia de 300k entre Vin y A0; 1V = 100k/(300k+220k+100k)*6.2V
  #define MAXV    6.20
  #define NITER   10.0
  int a0s = 0;  
  for( int i=0; i<NITER; i++ ){
    int a0 = analogRead( A0 );
    a0s = a0s + a0;
    yield();
    delay( 50 );
  }
  float voltaje = (float(a0s)/NITER)*(MAXV/1024.0);
  json["Voltaje"] = voltaje;
  //Serial.println( a0s/NITER );
  yield();

  // ---------------------------------------
  String buff;
  json.printTo( buff );
  json["sha1"] = sha1( "rcr[" + buff + "]" );
  json.printTo( Serial );
  Serial.println();
  yield();

  #if TESTING==1
  delay( 1000 );
  return;
  #endif

  // esperamos la conexión a la WiFi
  Serial.print( "Verificando WiFi <<" );
  Serial.print( WiFi.SSID() );
  Serial.print( ">> ." );
  unsigned long start = millis();
  while( ( millis() - start ) < 15UL * 1000UL )
  {
    Serial.print( "." );
    if(  WiFi.status() == WL_CONNECTED )
    {
      Serial.println( " : Conectado" );

      // conectamos con el broker MQTT
      Serial.print( "Conectando a servidor MQTT " );
      Serial.print( MQTT_SERVER );
      Serial.print( ":" );
      Serial.print( MQTT_PORT );
      Serial.print( " ... " );
      if( mqtt.connect( CLIENTID.c_str() ) )
      //if( mqtt.connect( (CLIENTID + json["sha1"].as<String>()).c_str() ) )
      {
        // publicamos
        Serial.println( "OK" );
        Serial.print( "Publicando en  " );
        Serial.print( MQTT_TOPIC );
        Serial.print( " ... " );
        String payload;
        json.printTo( payload );
        if( mqtt.publish( MQTT_TOPIC, payload.c_str() ) )
          Serial.println( "OK" );
        else
          Serial.println( "Error en Publish()" );
        mqtt.loop();
        delay(100);
        mqtt.disconnect();
      }
      else
        Serial.println( "Error" );
      
      break;
    } else {
      delay( 500 );
    }
  }

  Serial.println();
  Serial.println( "Reiniciando ..." );

  // repetimos el proceso con la pausa apropiada
  #if DEEP_SLEEP==0
  delay( DELAY );
  #else
  ESP.deepSleep( DELAY * 1000UL);
  #endif
}

// lee una línea hasta que se presiona enter
int readLine( char *buff, int len )
{
  return readLine( buff, len, 0 );
}

int readLine( char *buff, int len, long timeout )
{
  int i = 0;
  long t = timeout;

  while( true )
  {
    int c = Serial.read();
    yield();
    delay( 1 );

    if( c != -1 )
    {
      if( c == '\n' )
        break;
      else if( i < len )
        buff[ i++ ] = c;
    }

    if( timeout > 0 && --t <= 0 )
      break;
  }

  buff[ i ] = '\0';
  Serial.println();
  Serial.print( "Linea leida [[" );
  Serial.print( buff );
  Serial.println( "]]" );
  return strlen( buff );
}
