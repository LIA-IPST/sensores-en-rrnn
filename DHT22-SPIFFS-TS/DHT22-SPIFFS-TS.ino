// librerías mínimas requeridas
#include <Arduino.h>
#include <Wire.h>
#include <ESP8266WiFi.h>
#include <FS.h>
#include "ThingSpeak.h"
#include <Hash.h>

// ID de este nodo
String CLIENTID = "Node_" + String( ESP.getChipId() );

// objeto para la WiFi
WiFiClient wiFiClient;

// datos requerido para la conexion
#define CONFIG_FILE "config.txt"// archivo de configuracion
char WIFI_SSID[32];             // SSID de la WiFi
char WIFI_PASS[32];             // clave de la WiFi
char TS_SERVER[32];             // Servidor de ThingSpeak
char TS_PORT[6];                // Puerta de ThingSpeak
char TS_CHANNEL[20];            // Canal de ThingSpeak
char TS_WRITE_KEY[20];          // WRITE_KEY de ThingSpeak
char TIME_DELAY[10];            // milisegundos entre cada lectura

// control del código
#define TESTING         0       // 1: modo testing, 0: modo normal
#define DEEP_SLEEP      0       // 0: no usar DeepSleep, 1: usar DeepSleep
unsigned long DELAY;            // tiempo entre cada lectura en ms

// ------------------------------------------
// colocar aquí definiciones para su programa
// ------------------------------------------
#include "DHT.h"
DHT dht( D5, DHT22 );

// ------------------------------------------

void setup()
{
  // inicia monitor serial
  Serial.begin( 9600 );
  Serial.println();
  Serial.println();

  #if TESTING==0
  // iniciamos el sistema de archivos
  SPIFFS.begin();

  // esperamos 5 segundos por posible cambio en la configuración
  Serial.println();
  Serial.println();
  Serial.print( CLIENTID );
  Serial.print( " - Presione la tecla 's' y luego <enter> para reconfigurar:" );
  char line[8];

  if( readLine( line, 7, 5000 ) == 1 && line[0] == 's' && line[1] == 0 )
  {
    Serial.println();
    Serial.print( "Wifi SSID: " );
    if( readLine( WIFI_SSID, sizeof( WIFI_SSID ) - 1 ) )
    {
      Serial.print( "WiFi Clave: " );
      if( readLine( WIFI_PASS, sizeof( WIFI_PASS ) - 1 ) )
      {
        Serial.print( "TS Servidor: " );
        if( readLine( TS_SERVER, sizeof( TS_SERVER ) - 1 ) )
        {
          Serial.print( "TS Puerta: " );
          if( readLine( TS_PORT, sizeof( TS_PORT ) - 1 ) )
          {
            Serial.print( "TS Canal:" );
            if( readLine( TS_CHANNEL, sizeof( TS_CHANNEL ) - 1 ) )
            {
              Serial.print( "TS WRITE KEY:" );
              if( readLine( TS_WRITE_KEY, sizeof( TS_WRITE_KEY ) - 1 ) )
              {
                Serial.print( "Tiempo entre Lecturas (ms):" );
                if( readLine( TIME_DELAY, sizeof( TIME_DELAY ) - 1 ) )
                {
                  File f = SPIFFS.open( CONFIG_FILE, "w" );
                  f.println( WIFI_SSID );
                  f.println( WIFI_PASS );
                  f.println( TS_SERVER );
                  f.println( TS_PORT );
                  f.println( TS_CHANNEL );
                  f.println( TS_WRITE_KEY );
                  f.println( TIME_DELAY );
                  f.println();
                  f.close();
  
                  // inicia conexión a la WiFi
                  WiFi.setAutoConnect( true );
                  WiFi.mode( WIFI_STA );
                  WiFi.begin( WIFI_SSID, WIFI_PASS );
                }
              }
            }
          }
        }
      }
    }
  }

  // lee desde el archivo la configuracion requerida
  File f = SPIFFS.open( CONFIG_FILE, "r" );
  if( !f )
  {
    Serial.println();
    Serial.println( "No existe configuracion almacenada, el nodo se reiniciara en 3 segundos ..." );
    delay( 3000 );
    ESP.restart();
  }
  else
  {
    int n;
    if( n = f.readBytesUntil( '\n', WIFI_SSID,    sizeof( WIFI_SSID ) ) )     WIFI_SSID[n-1]=0;
    if( n = f.readBytesUntil( '\n', WIFI_PASS,    sizeof( WIFI_PASS ) ) )     WIFI_PASS[n-1]=0;
    if( n = f.readBytesUntil( '\n', TS_SERVER,    sizeof( TS_SERVER ) ) )     TS_SERVER[n-1]=0;
    if( n = f.readBytesUntil( '\n', TS_PORT,      sizeof( TS_PORT ) ) )       TS_PORT[n-1]=0;
    if( n = f.readBytesUntil( '\n', TS_CHANNEL,   sizeof( TS_CHANNEL ) ) )    TS_CHANNEL[n-1]=0;
    if( n = f.readBytesUntil( '\n', TS_WRITE_KEY, sizeof( TS_WRITE_KEY ) ) )  TS_WRITE_KEY[n-1]=0;
    if( n = f.readBytesUntil( '\n', TIME_DELAY,   sizeof( TIME_DELAY ) ) )    TIME_DELAY[n-1]=1000;
    f.close();

    DELAY = atol( TIME_DELAY );
    Serial.println( TS_CHANNEL );
    Serial.println( TS_WRITE_KEY );
    Serial.println( TIME_DELAY );
  }

  // inicia ThingSpeak
  ThingSpeak.begin( wiFiClient, TS_SERVER, atoi( TS_PORT ) );
  
  #endif

  // ---------------------------------------
  // colocar aquí inicialización de sensores
  // ---------------------------------------
  dht.begin();
  
  // ---------------------------------------

  // requerido para dar tiempo a los sensores
  Serial.println();
  Serial.println( "Iniciando lectura de sensores ..." );
  delay( 500 );
}

void loop()
{
  // Sensor DHT22
  float dhtTemperatura;
  float dhtHumedad;
  for( int i=0; i<3; i++ ){
    dhtTemperatura = dht.readTemperature();
    dhtHumedad = dht.readHumidity();
    yield();
    if( !isnan( dhtTemperatura ) && !isnan( dhtHumedad ) )
      break;
    Serial.println( "DHT22 reporta NAN" );
    yield();
    delay( 500 );
  }
  yield();
  
  // Voltaje de la bateria
  // Agregamos una resistencia de 300k entre Vin y A0; 1V = 100k/(300k+220k+100k)*6.2V
  #define MAXV    6.20
  #define NITER   10.0
  int a0s = 0;  
  for( int i=0; i<NITER; i++ ){
    int a0 = analogRead( A0 );
    a0s = a0s + a0;
    yield();
    delay( 50 );
  }
  float voltaje = (float(a0s)/NITER)*(MAXV/1024.0);
  //Serial.println( a0s/NITER );
  yield();

  // ---------------------------------------
  Serial.print( "ID: ");
  Serial.print( CLIENTID );  
  Serial.print( ", Temperatura: " );
  Serial.print( dhtTemperatura, 1 );
  Serial.print( "C" );
  Serial.print( ", Humedad:" );
  Serial.print( dhtHumedad, 1 );
  Serial.print( "%" );
  Serial.print( ", Voltaje:" );
  Serial.println( voltaje, 1 );
  yield();

  #if TESTING==1
  delay( 1000 );
  return;
  #endif

  // esperamos la conexión a la WiFi
  Serial.print( "Verificando WiFi <<" );
  Serial.print( WiFi.SSID() );
  Serial.print( ">> ." );
  unsigned long start = millis();
  while( ( millis() - start ) < 15UL * 1000UL )
  {
    Serial.print( "." );
    if(  WiFi.status() == WL_CONNECTED )
    {
     Serial.println( " : Conectado." );

      // enviamos a ThingSpeak
      Serial.print( "Enviando a " );
      Serial.print( TS_SERVER );
      Serial.print( ":" );
      Serial.print( TS_PORT );
      Serial.print( "/channel=" );
      Serial.print( TS_CHANNEL );
      Serial.print( "&key=" );
      Serial.print( TS_WRITE_KEY );
      Serial.print( " ... " );

      ThingSpeak.setField( 1, CLIENTID );
      ThingSpeak.setField( 2, dhtTemperatura );
      ThingSpeak.setField( 3, dhtHumedad );
      ThingSpeak.setField( 4, voltaje );
      Serial.print( ThingSpeak.writeFields( atol( TS_CHANNEL ), TS_WRITE_KEY ) );
      break;    
     } else {
      delay( 500 );
    }
  }

  Serial.println();
  Serial.print( "Reiniciando en " );
  Serial.print( DELAY );
  Serial.println( " ..." );

  // repetimos el proceso con la pausa apropiada
  #if DEEP_SLEEP==0
  delay( DELAY );
  #else
  ESP.deepSleep( DELAY * 1000UL);
  #endif
}

// lee una línea hasta que se presiona enter
int readLine( char *buff, int len )
{
  return readLine( buff, len, 0 );
}

int readLine( char *buff, int len, long timeout )
{
  int i = 0;
  long t = timeout;

  while( true )
  {
    int c = Serial.read();
    yield();
    delay( 1 );

    if( c != -1 )
    {
      if( c == '\n' )
        break;
      else if( i < len )
        buff[ i++ ] = c;
    }

    if( timeout > 0 && --t <= 0 )
      break;
  }

  buff[ i ] = '\0';
  Serial.println();
  Serial.print( "Linea leida [[" );
  Serial.print( buff );
  Serial.println( "]]" );
  return strlen( buff );
}
