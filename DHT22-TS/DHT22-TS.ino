// librerías mínimas requeridas
#include <Arduino.h>
#include <Wire.h>
#include <ESP8266WiFi.h>
#include "ThingSpeak.h"

// ID de este nodo
String CLIENTID = "Node_" + String( ESP.getChipId() );

// objeto para la WiFi
WiFiClient wiFiClient;

// datos requerido para la conexion
#define WIFI_SSID     "xxx"                 // SSID de la WiFi
#define WIFI_PASS     "xxx"          // clave de la WiFi
#define TS_SERVER     "192.168.6.254"           // Servidor de ThingSpeak
#define TS_PORT       18080                     // Puerta de ThingSpeak
#define TS_CHANNEL    1                         // Canal de ThingSpeak
#define TS_WRITE_KEY  "xxx"        // WRITE_KEY de ThingSpeak

// control del código
#define TESTING       0                         // 1: modo testing, 0: modo normal
#define DEEP_SLEEP    0                         // 0: no usar DeepSleep, 1: usar DeepSleep
#define TIME_DELAY    10000UL                   // milisegundos entre cada lectura

// ------------------------------------------
// colocar aquí definiciones para su programa
// ------------------------------------------
#include "DHT.h"
DHT dht( D5, DHT22 );

// ------------------------------------------

void setup()
{
  // inicia monitor serial
  Serial.begin( 9600 );
  Serial.println();
  Serial.println();

  #if TESTING==0

  // inicia conexión a la WiFi
  WiFi.setAutoConnect( true );
  WiFi.mode( WIFI_STA );
  WiFi.begin( WIFI_SSID, WIFI_PASS );

  // inicia ThingSpeak
  ThingSpeak.begin( wiFiClient, TS_SERVER, TS_PORT );
  
  #endif

  // ---------------------------------------
  // colocar aquí inicialización de sensores
  // ---------------------------------------
  dht.begin();
  
  // ---------------------------------------

  // requerido para dar tiempo a los sensores
  Serial.println();
  Serial.println( "Iniciando lectura de sensores ..." );
  delay( 500 );
}

void loop()
{
  // Sensor DHT22
  float dhtTemperatura;
  float dhtHumedad;
  for( int i=0; i<3; i++ ){
    dhtTemperatura = dht.readTemperature();
    dhtHumedad = dht.readHumidity();
    yield();
    if( !isnan( dhtTemperatura ) && !isnan( dhtHumedad ) )
      break;
    Serial.println( "DHT22 reporta NAN" );
    yield();
    delay( 500 );
  }
  yield();
  
  // Voltaje de la bateria
  // Agregamos una resistencia de 300k entre Vin y A0; 1V = 100k/(300k+220k+100k)*6.2V
  #define MAXV    6.20
  #define NITER   10.0
  int a0s = 0;  
  for( int i=0; i<NITER; i++ ){
    int a0 = analogRead( A0 );
    a0s = a0s + a0;
    yield();
    delay( 50 );
  }
  float voltaje = (float(a0s)/NITER)*(MAXV/1024.0);
  //Serial.println( a0s/NITER );
  yield();

  // ---------------------------------------
  Serial.print( "ID: ");
  Serial.print( CLIENTID );  
  Serial.print( ", Temperatura: " );
  Serial.print( dhtTemperatura, 1 );
  Serial.print( "C" );
  Serial.print( ", Humedad:" );
  Serial.print( dhtHumedad, 1 );
  Serial.print( "%" );
  Serial.print( ", Voltaje:" );
  Serial.println( voltaje, 1 );
  yield();

  #if TESTING==1
  delay( 1000 );
  return;
  #endif

  // esperamos la conexión a la WiFi
  Serial.print( "Verificando WiFi <<" );
  Serial.print( WiFi.SSID() );
  Serial.print( ">> ." );
  unsigned long start = millis();
  while( ( millis() - start ) < 15UL * 1000UL )
  {
    Serial.print( "." );
    if(  WiFi.status() == WL_CONNECTED )
    {
     Serial.println( " : Conectado." );

      // enviamos a ThingSpeak
      Serial.print( "Enviando a " );
      Serial.print( TS_SERVER );
      Serial.print( ":" );
      Serial.print( TS_PORT );
      Serial.print( "/channel=" );
      Serial.print( TS_CHANNEL );
      Serial.print( "&key=" );
      Serial.print( TS_WRITE_KEY );
      Serial.print( " ... " );

      ThingSpeak.setField( 1, CLIENTID );
      ThingSpeak.setField( 2, dhtTemperatura );
      ThingSpeak.setField( 3, dhtHumedad );
      ThingSpeak.setField( 4, voltaje );
      Serial.print( ThingSpeak.writeFields( TS_CHANNEL, TS_WRITE_KEY ) );
      break;    
     } else {
      delay( 500 );
    }
  }

  Serial.println();
  Serial.print( "Reiniciando en " );
  Serial.print( TIME_DELAY );
  Serial.println( " ..." );

  // repetimos el proceso con la pausa apropiada
  #if DEEP_SLEEP==0
  delay( TIME_DELAY );
  #else
  ESP.deepSleep( TIME_DELAY * 1000UL);
  #endif
}

// lee una línea hasta que se presiona enter
int readLine( char *buff, int len )
{
  return readLine( buff, len, 0 );
}

int readLine( char *buff, int len, long timeout )
{
  int i = 0;
  long t = timeout;

  while( true )
  {
    int c = Serial.read();
    yield();
    delay( 1 );

    if( c != -1 )
    {
      if( c == '\n' )
        break;
      else if( i < len )
        buff[ i++ ] = c;
    }

    if( timeout > 0 && --t <= 0 )
      break;
  }

  buff[ i ] = '\0';
  Serial.println();
  Serial.print( "Linea leida [[" );
  Serial.print( buff );
  Serial.println( "]]" );
  return strlen( buff );
}
