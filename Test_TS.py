import time
import random
import urllib
import urllib.parse
import urllib.request

baseURL = 'http://api.thingspeak.com/update?api_key=CX98XANY3K3AGUAO&field1='

class MyThingSpeak:
    def __init__( self, server, port, writeKey=None, readKey=None ):
        self.baseURL = 'http://{0}:{1}/'.format( server, port )
        self.writeKey = writeKey
        self.readKey = readKey

    def publish( self, data ):
        url = self.baseURL + 'update?api_key=' + self.writeKey

        i = 1
        for d in data:
            d = urllib.parse.quote( '%s' % d )
            url = url + '&field{0}={1}'.format( i, d )
            i = i + 1
        print( url )
        f = urllib.request.urlopen( url )
        f.read()
        f.close()

myts = MyThingSpeak( 'tron-ipst.freeddns.org', 18080, '0NA0DYH3LORKSLHO' )
for i in range( 10 ):
    temperatura = random.randrange( 0, 500 )/10.0
    humedad = random.randrange( 0, 1000 )/10.0
    voltaje = random.randrange( 0, 47 )/10.0
    data = ( 'NODO_001', temperatura, humedad, voltaje )
    print( data )
    myts.publish( data )
    time.sleep( 5 )
